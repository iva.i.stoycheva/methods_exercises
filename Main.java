package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here

//        int[] array2 = new int[5];
//        array2[0]=1;
//        System.out.println(array3[2]);
//        System.out.println(array3.length);
//declare some variables
        byte centuries = 20;
        short years = 2000;
        int days = 730480;
        long hours = 17531520;
// Print the result on the console
        System.out.println(centuries + " centuries is " + years +
                " years, or " + days + " days, or " + hours + " hours.");

        // Declare some variables
        float floatPI = 3.141592653589793238f;
        double doublePI = 3.141592653589793238;
// Print the result on the console
        System.out.println("Float PI is: " + floatPI);
        System.out.println("Double PI is: " + doublePI);

        // Declare some variables
        float sum =
                0.1f + 0.1f + 0.1f + 0.1f + 0.1f +
                        0.1f + 0.1f + 0.1f + 0.1f + 0.1f;
        float num = 1.0f;
// Is sum equal to num
        boolean equal = (num == sum);
// Print the result ot the console
        System.out.println(
                "num = " + num + " sum = " + sum + " equal = " + equal);
    }
}
