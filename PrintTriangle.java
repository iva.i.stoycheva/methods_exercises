package com.company;

import java.util.Scanner;

public class PrintTriangle {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);// read from console input
        int number = sc.nextInt();
        for(int row = 1; row <= number; row++) {
            for (int col = 1; col <= row ; col++) {
                System.out.print(col + " ");
            }
            System.out.println();
        }
    }
}
