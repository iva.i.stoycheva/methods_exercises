package com.company;

import java.util.Locale;

public class PrintingFormattedStrings {
    public static void main(String[] args) {
        String name ="Iva";
        int age = 30;
        String town = "Plovdiv";
        System.out.printf("My name is %s. \n", name);
        // output: my name is iva

        System.out.printf("my name is %S. \n", name);

        System.out.printf(
                "%1$s is big town. \n" +
                "%2$s lives in %1$s \n" +
                "%2$s is %3$d years old. \n", town, name, age);

        int a = 2, b = 3;
        System.out.printf("%d + %d = ", a, b);
        System.out.printf("%d\n", (a+b));


        float pi = 3.1415206f;
        System.out.printf("%.2f%n", pi);

        Locale.setDefault(Locale.UK);
        System.out.println("Locale: " + Locale.getDefault().toString());

    }
}
