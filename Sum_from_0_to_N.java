package com.company;

import java.util.Scanner;

public class Sum_from_0_to_N {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("The number n = ");
        int n = sc.nextInt();
        int number = 1;
        int sum = 1;

        System.out.print("The sum = 1");
        while (n > number){
            number++;
            sum += number;
            System.out.printf("+%d", + number);
        }
        System.out.printf(" = %d%n", sum);
    }
}
